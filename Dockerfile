FROM maven:3.9.6-eclipse-temurin-21-alpine as builder

WORKDIR /app

COPY pom.xml /app/pom.xml
COPY src /app/src

RUN mvn clean package -DskipTests


FROM openjdk:21-ea-1-jdk-slim

WORKDIR /app

COPY --from=builder /app/target/call-github-api-0.0.1-SNAPSHOT.jar /app/app.jar

EXPOSE 8080

CMD ["java", "-jar", "app.jar"]
