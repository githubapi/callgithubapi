# call-github-api

A simple application to interact with the GitHub API.

## To run the application:

### Prerequisites:
- Docker installed on your local machine

### Steps:
1. Clone the repository to your local machine:
   ```bash
   git clone git@gitlab.com:githubapi/callgithubapi.git
2. Navigate to the repository directory: cd call-github-api
3. Build the Docker image: docker build -t call-github-api:v1 .
4. Run the Docker container: docker run --name call-github-api --rm -p 8080:8080 call-github-api:v1

### Your instance of the application is up and running!

You can interact with the application using Postman or your web browser.

#### Using Postman:
1. Open Postman.
2. Set the request type to GET.
3. Enter the following URL in the address bar:
   http://localhost:8080/api/github-response/{username}
   Replace `{username}` with the GitHub username you want to query.
4. Send the request to the server.
5. You will receive a JSON response containing the GitHub repositories information.

#### Using a Web Browser:
1. Open your favorite web browser.
2. Enter the following URL in the address bar:
   http://localhost:8080/api/github-response/{username}
   Replace `{username}` with the GitHub username you want to query.
3. Hit Enter to send the request to the server.
4. You will receive a JSON response containing the GitHub repositories information.

##### Once you have exceeded rate limit:

1. Generate a GitHub personal access token from your GitHub account.
2. Paste the generated token into the URL as follows:
   http://localhost:8080/api/github-response/{username}/{token}
   Replace {username} with the GitHub username you want to query and {token} with your generated personal access token.
3. Send the request again using either Postman or your web browser.
4. You should now receive a JSON response containing the GitHub repositories information without encountering rate limit errors.
   You should now receive a JSON response containing the GitHub repositories information without encountering rate limit errors.



