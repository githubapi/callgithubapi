package com.kozuchowski.callgithubapi.repository;

import com.kozuchowski.callgithubapi.exception.RateLimitExceededException;
import com.kozuchowski.callgithubapi.exception.UserNotFoundException;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class GithubCallerImplTest {

    @Autowired
    RestTemplate restTemplate;
    @Autowired
    GithubCallerImpl githubCaller;

    @Test
    void shouldReturnResponse() throws UserNotFoundException, RateLimitExceededException {
        String response = githubCaller.callGithubApi("https://api.github.com/users/testUser/repos");

        assertNotNull(response);
    }
    @Test
    void shouldThrowUserNotFounException() {
        assertThrows(UserNotFoundException.class, () -> githubCaller.callGithubApi("https://api.github.com/users/nonExistingUser/repos"));
    }

}