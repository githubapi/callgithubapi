package com.kozuchowski.callgithubapi.service;

import com.kozuchowski.callgithubapi.model.GithubResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
class GithubClientImplTest {

    @Autowired
    GithubClientImpl githubClient;

    @Test
    void shouldReturnGithubResponse() throws Exception {
        List<GithubResponse> responses = githubClient.getApiResponse("testUser");
        assertNotNull(responses);

    }
}