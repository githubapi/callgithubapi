package com.kozuchowski.callgithubapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CallGithubApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CallGithubApiApplication.class, args);
	}

}
