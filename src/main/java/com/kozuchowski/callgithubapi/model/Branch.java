package com.kozuchowski.callgithubapi.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Branch {
    private String name;
    private Commit commit;

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class Commit {
        private String sha;
    }
}