package com.kozuchowski.callgithubapi.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GithubResponse {
    private String repositoryName;
    private String ownerLogin;
    private List<Branch> branches;
}
