package com.kozuchowski.callgithubapi.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kozuchowski.callgithubapi.exception.RateLimitExceededException;
import com.kozuchowski.callgithubapi.exception.UserNotFoundException;
import com.kozuchowski.callgithubapi.model.Branch;
import com.kozuchowski.callgithubapi.model.GithubResponse;

import java.util.List;

public interface GithubClient {
    List<GithubResponse> getApiResponse(String username) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException;
    List<GithubResponse> getApiResponse(String username, String token) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException;

    List<Branch> getBranchNameAndLastCommitSha(String branchesUrl) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException;
}
