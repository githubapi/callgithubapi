package com.kozuchowski.callgithubapi.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kozuchowski.callgithubapi.exception.RateLimitExceededException;
import com.kozuchowski.callgithubapi.exception.UserNotFoundException;
import com.kozuchowski.callgithubapi.model.Branch;
import com.kozuchowski.callgithubapi.model.GithubResponse;
import com.kozuchowski.callgithubapi.repository.GithubCallerImpl;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;


import java.util.ArrayList;
import java.util.List;

@Service
public class GithubClientImpl implements GithubClient {




    public GithubClientImpl(GithubCallerImpl githubCaller, ObjectMapper objectMapper) {
        this.githubCaller = githubCaller;
        this.objectMapper = objectMapper;
    }

    private final GithubCallerImpl githubCaller;
    private final ObjectMapper objectMapper;
    @Value("${github.api.url}")
    private String apiUrl;
    private List<GithubResponse> responses = new ArrayList<>();


    @Override
    public List<GithubResponse> getApiResponse(String username) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        String apiRepositoriesUrl = apiUrl + username + "/repos";
        String repositoriesResponse = githubCaller.callGithubApi(apiRepositoriesUrl);
        return getGithubResponses(repositoriesResponse, null);
    }

    @Override
    public List<GithubResponse> getApiResponse(String username, String token) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        String apiRepositoriesUrl = apiUrl + username + "/repos";
        String repositoriesResponse = githubCaller.callGithubApi(apiRepositoriesUrl, token);
        return getGithubResponses(repositoriesResponse, token);
    }

    private List<GithubResponse> getGithubResponses(String repositoriesResponse, String token) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        List<GithubRepository> repositories = objectMapper.readValue(repositoriesResponse, new TypeReference<>() {});
        for (GithubRepository repository : repositories) {
            List<Branch> branches = new ArrayList<>();
            if(repository.isFork()) {
                continue;
            }
            if(token != null) {
                branches = getBranchNameAndLastCommitSha
                        (repository.getBranches_url().replace("{/branch}", ""), token);
            }
            if(token == null) {
                branches = getBranchNameAndLastCommitSha
                        (repository.getBranches_url().replace("{/branch}", ""));
            }

                GithubResponse githubResponse = new GithubResponse(repository.getName(),
                        repository.getOwner().getLogin(), branches);
                responses.add(githubResponse);

        }

        return responses;
    }


    @Override
    public List<Branch> getBranchNameAndLastCommitSha(String branchesUrl) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        String branchesResponse = githubCaller.callGithubApi(branchesUrl);
        List<Branch> branches = objectMapper.readValue(branchesResponse, new TypeReference<>() {});
        return branches;
    }

    public List<Branch> getBranchNameAndLastCommitSha(String branchesUrl, String token) throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        String branchesResponse = githubCaller.callGithubApi(branchesUrl, token);
        List<Branch> branches = objectMapper.readValue(branchesResponse, new TypeReference<>() {});
        return branches;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    private static class GithubRepository {
        private String name;
        private Owner owner;
        private boolean fork;
        private String branches_url;


        @Getter
        @Setter
        @AllArgsConstructor
        @NoArgsConstructor
        private static class Owner {
            private String login;
        }
    }

}
