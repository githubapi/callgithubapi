package com.kozuchowski.callgithubapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kozuchowski.callgithubapi.exception.RateLimitExceededException;
import com.kozuchowski.callgithubapi.exception.UserNotFoundException;
import com.kozuchowski.callgithubapi.model.GithubResponse;
import com.kozuchowski.callgithubapi.service.GithubClientImpl;
import jakarta.validation.constraints.Size;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class GithubController {

    public GithubController(GithubClientImpl githubService) {
        this.githubService = githubService;
    }

    private final GithubClientImpl githubService;

    @GetMapping("/api/github-response/{username}")
    public List<GithubResponse> getGithubResponse(@PathVariable @NotBlank @Size(max = 500) String username)
            throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        return  githubService.getApiResponse(username);
    }

    @GetMapping("/api/github-response/{username}/{token}")
    public List<GithubResponse> getGithubResponseAuthenticated(@PathVariable @NotBlank @Size(max = 500) String username,
                                                               @PathVariable @NotBlank String token)
            throws JsonProcessingException, UserNotFoundException, RateLimitExceededException {
        return  githubService.getApiResponse(username, token);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ResponseEntity<Map<String, Object>> handleUserNotFoundException(UserNotFoundException ex) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", HttpStatus.NOT_FOUND.value());
        response.put("message", ex.getMessage().split(":")[0]);
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
    }

    @ExceptionHandler(RateLimitExceededException.class)
    public ResponseEntity<Map<String, Object>> handleRateLimitExceededException(RateLimitExceededException ex) {
        Map<String, Object> response = new HashMap<>();
        response.put("status", HttpStatus.FORBIDDEN.value());
        response.put("message", ex.getMessage().split(":")[0]);
        return ResponseEntity.status(HttpStatus.FORBIDDEN).body(response);
    }


}
