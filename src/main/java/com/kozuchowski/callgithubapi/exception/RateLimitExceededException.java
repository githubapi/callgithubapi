package com.kozuchowski.callgithubapi.exception;

public class RateLimitExceededException extends Exception {

    public RateLimitExceededException() {};
    public RateLimitExceededException(String message) {
        super(message);
    }

    public RateLimitExceededException(Throwable cause) {
        super(cause);
    }

    public RateLimitExceededException(String message, Throwable cause) {
        super(message, cause);
    }
}
