package com.kozuchowski.callgithubapi.repository;

import com.kozuchowski.callgithubapi.exception.RateLimitExceededException;
import com.kozuchowski.callgithubapi.exception.UserNotFoundException;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;


@Service
public class GithubCallerImpl implements GithubCaller {

    private final RestTemplate restTemplate;
    private static int apiCallsCounter = 0;

    public GithubCallerImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    public String callGithubApi(String url) throws RateLimitExceededException, UserNotFoundException {
        return callApi(url);
    }

    @Override
    public String callGithubApi(String url, String token) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(token);
        return callApi(url, headers);

    }

    private String callApi(String url, HttpHeaders headers) {
        try {
            HttpEntity<String> httpEntity = new HttpEntity<>(headers);
            ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.GET, httpEntity, String.class);
            return responseEntity.getBody();
        } catch (Exception e) {
            return null;
        }
    }


    private String callApi(String url) throws RateLimitExceededException, UserNotFoundException {
        apiCallsCounter++;
        try {
            return restTemplate.getForObject(url, String.class);
        } catch (HttpClientErrorException e) {
            if (e.getStatusCode().value() == 403) {
                throw new RateLimitExceededException("API rate limit exceeded: " + e.getMessage());
            }
            if (e.getStatusCode().value() == 404) {
                throw new UserNotFoundException("User not found: " + e.getMessage());
            }
            return null;
        }
    }

}
