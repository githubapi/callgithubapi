package com.kozuchowski.callgithubapi.repository;

import com.kozuchowski.callgithubapi.exception.RateLimitExceededException;
import com.kozuchowski.callgithubapi.exception.UserNotFoundException;

public interface GithubCaller {
    String callGithubApi(String url) throws RateLimitExceededException, UserNotFoundException;
    String callGithubApi(String url, String token) throws RateLimitExceededException, UserNotFoundException;
}
